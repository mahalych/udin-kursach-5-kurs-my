﻿namespace WinFormsAppManchesterCode
{
	using System;
	using System.IO;
	using System.Text;
	using System.Windows.Forms;
	using ManchesterCode;

	public partial class MainForm : Form
	{
		public MainForm() {
			InitializeComponent();
		}

		private void loadFromFileButton_Click(object sender, EventArgs e) {
			using (var ofd = new OpenFileDialog()) {
				ofd.Filter = "*.txt|*.txt";
				if (ofd.ShowDialog() == DialogResult.OK) {
					dataTextBox.Text = File.ReadAllText(ofd.FileName, Encoding.UTF8);
				}
			}
		}

		private void encodeButton_Click(object sender, EventArgs e) {
			string dataToEncode = dataTextBox.Text;
			if (String.IsNullOrEmpty(dataToEncode)) {
				MessageBox.Show("Нет данных подлежащих кодированию!");
			}
			var manch = new Manchester();
			string encoded = manch.Encode(dataToEncode);
			encodedDataTextBox.Text = encoded;
		}

		private void decodeButton_Click(object sender, EventArgs e) {
			string encoded = encodedDataTextBox.Text;
			if (String.IsNullOrEmpty(encoded)) {
				MessageBox.Show("Нет закодированных данных, которые возможно было бы раскодировать!");
			}
			var manch = new Manchester();
			decodedDataTextBox.Text = manch.Decode(encoded, null);
		}

		private void dataTextBox_TextChanged(object sender, EventArgs e) {
			encodedDataTextBox.Enabled = String.IsNullOrEmpty(dataTextBox.Text);
		}
	}
}
