﻿namespace WinFormsAppManchesterCode
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.dataTextBox = new System.Windows.Forms.TextBox();
			this.encodeButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.encodedDataTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.decodedDataTextBox = new System.Windows.Forms.TextBox();
			this.decodeButton = new System.Windows.Forms.Button();
			this.loadFromFileButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// dataTextBox
			// 
			this.dataTextBox.Location = new System.Drawing.Point(194, 17);
			this.dataTextBox.Multiline = true;
			this.dataTextBox.Name = "dataTextBox";
			this.dataTextBox.Size = new System.Drawing.Size(282, 91);
			this.dataTextBox.TabIndex = 0;
			this.dataTextBox.TextChanged += new System.EventHandler(this.dataTextBox_TextChanged);
			// 
			// encodeButton
			// 
			this.encodeButton.Location = new System.Drawing.Point(482, 129);
			this.encodeButton.Name = "encodeButton";
			this.encodeButton.Size = new System.Drawing.Size(104, 45);
			this.encodeButton.TabIndex = 1;
			this.encodeButton.Text = "Закодировать";
			this.encodeButton.UseVisualStyleBackColor = true;
			this.encodeButton.Click += new System.EventHandler(this.encodeButton_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(43, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(129, 40);
			this.label1.TabIndex = 2;
			this.label1.Text = "Данные, которые нужно закодировать";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(43, 145);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(113, 35);
			this.label2.TabIndex = 4;
			this.label2.Text = "Закодированные данные";
			// 
			// encodedDataTextBox
			// 
			this.encodedDataTextBox.Location = new System.Drawing.Point(194, 114);
			this.encodedDataTextBox.Multiline = true;
			this.encodedDataTextBox.Name = "encodedDataTextBox";
			this.encodedDataTextBox.Size = new System.Drawing.Size(282, 91);
			this.encodedDataTextBox.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(43, 243);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(113, 35);
			this.label3.TabIndex = 6;
			this.label3.Text = "Раскодированные данные";
			// 
			// decodedDataTextBox
			// 
			this.decodedDataTextBox.Enabled = false;
			this.decodedDataTextBox.Location = new System.Drawing.Point(194, 211);
			this.decodedDataTextBox.Multiline = true;
			this.decodedDataTextBox.Name = "decodedDataTextBox";
			this.decodedDataTextBox.Size = new System.Drawing.Size(282, 91);
			this.decodedDataTextBox.TabIndex = 5;
			// 
			// decodeButton
			// 
			this.decodeButton.Location = new System.Drawing.Point(482, 233);
			this.decodeButton.Name = "decodeButton";
			this.decodeButton.Size = new System.Drawing.Size(104, 45);
			this.decodeButton.TabIndex = 7;
			this.decodeButton.Text = "Раскодировать";
			this.decodeButton.UseVisualStyleBackColor = true;
			this.decodeButton.Click += new System.EventHandler(this.decodeButton_Click);
			// 
			// loadFromFileButton
			// 
			this.loadFromFileButton.Location = new System.Drawing.Point(482, 43);
			this.loadFromFileButton.Name = "loadFromFileButton";
			this.loadFromFileButton.Size = new System.Drawing.Size(104, 45);
			this.loadFromFileButton.TabIndex = 8;
			this.loadFromFileButton.Text = "Загрузить из файла";
			this.loadFromFileButton.UseVisualStyleBackColor = true;
			this.loadFromFileButton.Click += new System.EventHandler(this.loadFromFileButton_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(598, 358);
			this.Controls.Add(this.loadFromFileButton);
			this.Controls.Add(this.decodeButton);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.decodedDataTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.encodedDataTextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.encodeButton);
			this.Controls.Add(this.dataTextBox);
			this.Name = "MainForm";
			this.Text = "Manchester code";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox dataTextBox;
		private System.Windows.Forms.Button encodeButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox encodedDataTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox decodedDataTextBox;
		private System.Windows.Forms.Button decodeButton;
		private System.Windows.Forms.Button loadFromFileButton;
	}
}

