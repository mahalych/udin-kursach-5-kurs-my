﻿namespace ManchesterCode
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Text.RegularExpressions;

	public class Manchester
	{
		private string ToBinString(byte[] input) {
			return String.Join("", input.Select(@byte => Convert.ToString(@byte, 2).PadLeft(8, '0')));
		}

		private byte[] ToByteArr(string input) {
			if (new Regex(@"[^01]").IsMatch(input)) {
				throw new ArgumentException("there can be only '0' and '1' characters");
			}
			int nBytes = (int)Math.Ceiling(input.Length / 8m);
			return Enumerable.Range(0, nBytes)
				.Select(i => input.Substring(8 * i, Math.Min(8, input.Length - 8 * i)))
				.Select(b => Convert.ToByte(b, 2))
				.ToArray();
		}

		private string Encode(string input) {
			var sb = new StringBuilder();
			foreach (char bit in input) {
				if (bit == '1') {
					sb.Append("10");
				} else {
					sb.Append("01");
				}
			}
			string result = sb.ToString();
			return result;
		}

		public string Encode(string input, Encoding encoding = null) {
			encoding = encoding ?? Encoding.UTF8;
			return Encode(ToBinString(encoding.GetBytes(input)));
		}

		public string Encode(byte[] input) {
			return Encode(ToBinString(input));
		}

		public string Decode(string input, Encoding encoding) {
			encoding = encoding ?? Encoding.UTF8;
			return encoding.GetString(Decode(input));
		}

		public byte[] Decode(string input) {
			if (new Regex(@"[^01]").IsMatch(input)) {
				throw new ArgumentException("there can be only '0' and '1' characters");
			}
			IEnumerable<string> twoBitChuncks = Enumerable.Range(0, input.Length / 2)
				.Select(i => input.Substring(2 * i, 2));
			var sb = new StringBuilder();
			foreach (string twoBit in twoBitChuncks) {
				if (twoBit == "10") {
					sb.Append("1");
				} else {
					sb.Append("0");
				}
			}
			string result = sb.ToString();
			return ToByteArr(result);
		}
	}
}
