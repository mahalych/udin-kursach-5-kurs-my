﻿namespace Manchester
{
	using System;
	using System.IO;
	using System.Text;
	using ManchesterCode;

	class Program
	{
		static void Main(string[] args) {
			Console.BackgroundColor = ConsoleColor.White;
			Console.ForegroundColor = ConsoleColor.Black;
			Console.Clear();
			Console.InputEncoding = Encoding.GetEncoding(1251);
			Console.OutputEncoding = Encoding.GetEncoding(1251);
			bool breakCycle = false;
			bool encode = true;
			string input = String.Empty;
			do {
				Console.WriteLine("1 - закодировать данные из клавиатуры");
				Console.WriteLine("2 - закодировать данные из txt файла");
				Console.WriteLine("3 - раскодировать данные из клавиатуры");
				Console.WriteLine("4 - раскодировать данные из txt файла");
				Console.WriteLine("5 - выход");
				string choise = Console.ReadLine();
				switch (choise) {
					case "1":
						Console.Write("Введите данные, подлежащие кодированию: ");
						input = Console.ReadLine();
						breakCycle = true;
						break;
					case "2": {
							Console.WriteLine("Введите имя файла:");
							string fileName = Console.ReadLine();
							if (!File.Exists(fileName)) {
								Console.WriteLine("Файл не существует!");
								break;
							}
							input = File.ReadAllText(fileName);
							breakCycle = true;
							break;
						}
					case "3":
						Console.Write("Введите данные, подлежащие раскодированию: ");
						input = Console.ReadLine();
						breakCycle = true;
						encode = false;
						break;
					case "4": {
							Console.WriteLine("Введите имя файла:");
							string fileName = Console.ReadLine();
							if (!File.Exists(fileName)) {
								Console.WriteLine("Файл не существует!");
								break;
							}
							input = File.ReadAllText(fileName);
							breakCycle = true;
							encode = false;
							break;
						}
					case "5":
						Console.ReadKey();
						return;
					default:
						Console.WriteLine("Неизвестная комманда!");
						break;
				}
			} while (!breakCycle);
			var manch = new Manchester();
			string encoded = String.Empty;
			if (encode) {
				encoded = manch.Encode(input, Encoding.UTF8);
				Console.WriteLine($"Закодированные данные: {encoded}");
			} else {
				encoded = input;
			}
			string decoded = manch.Decode(encoded, Encoding.UTF8);
			Console.WriteLine($"Раскодированные данные: {decoded}");
			Console.ReadKey();
		}
	}
}
